FROM php:8.3.3-apache

# Set the desired document root for a Laravel project
ENV APACHE_DOCUMENT_ROOT /var/www/html/public
ENV HOME="/root"
# Set environment variables for nvm
ENV NVM_DIR /root/.nvm
ENV NODE_VERSION 21.7.1

# Update the Apache configuration to use the new document root
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

# Enable mod_rewrite
RUN a2enmod rewrite

# Set the working directory in the container
WORKDIR /var/www/html

# Install dependencies
RUN apt-get update && \
    apt-get install -y \
    zlib1g-dev \
    libzip-dev \
    libssl-dev \
    unzip \
    curl

# Install nvm (Node Version Manager)
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash \
&& . $NVM_DIR/nvm.sh \
&& nvm install $NODE_VERSION \
&& nvm alias default $NODE_VERSION \
&& nvm use default

#Setup node path
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

#install mongo db
RUN pecl install mongodb && docker-php-ext-enable mongodb

RUN docker-php-ext-install zip

#Install composer
RUN curl -sS https://getcomposer.org/installer | php -- \
     --install-dir=/usr/local/bin --filename=composer

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Copy the project files into the container - Enable when setup server
#COPY . .

# Install Laravel dependencies - Enable when setup server
#RUN composer install --no-dev --optimize-autoloader

#Run composer require mongodb/laravel-mongodb

# Set folder permissions if needed - Enable when setup server
#RUN chown -R www-data:www-data storage bootstrap/cache

# Expose port 80 for Apache
EXPOSE 80

# Start Apache
CMD ["apache2-foreground"]
