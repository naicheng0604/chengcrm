<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PublicController extends Controller
{
    /**
    */
    public function home(Request $request) {

        return Inertia::render('Web/Home', [
        ]);
    }
}
